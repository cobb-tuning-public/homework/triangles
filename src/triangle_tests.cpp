#include "gtest/gtest.h"
#include "triangle.h"

TEST(triangle, equilateral)
{
    ASSERT_TRUE(isValidTriangle(3,3,3));
}

TEST(triangle, scalene)
{
    ASSERT_TRUE(isValidTriangle(1,2,4));
}

TEST(triangle, isosceles)
{
    ASSERT_TRUE(isValidTriangle(2,2,5));
}