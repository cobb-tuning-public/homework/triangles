# COBB Homework: Triangles
A triangle is a valid triangle, If and only If, the sum of any two sides of a triangle is greater than the third side.

The code inside `src/triangle.cpp` function `isValidTriangle` is used to calculate if the 3 integers provided can result in the construction of a valid triangle. The code currently does not pass all of it's tests.

## How To Build
```
git clone --recursive git@gitlab.com:cobb-tuning-public/homework/triangles.git
cd triangles
mkdir build
cd build
cmake ..
cmake --build .
./tests # or .\Debug\tests.exe
```

## Deliverables
* Privately fork this repo and commit your changes there
* Make changes required to make all tests pass
* Provide any other bugs that were not covered by the original tests
* Provide additional tests if additional bugs were identified

### Restrictions
* Do not change the function prototype
